# Probe App
- Usage

  | URI   |      Method      |  Description | Sample Response |
  |----------|:-------------:|------:|------:|
  | /hello|  GET| Response 200 OK |{ "status":"ok"}|
  | /hello|  GET| Response 503 Service Unavailable|{ "status":"down"}|
  | /stop|  GET| Set /hello to return 503 Service Unavailable|{ "status":"ok"}|
  | /start|  GET| Set /hello to return 200 OK|{ "status":"ok"}|
  | /q/health/live|  GET| Liveness|{"checks": [],"status": "UP"}|
  | /q/health/ready|  GET| Readiness|{"checks": [{"name": "Ready","status": "UP"}], "status": "UP"| 
- Build native binary

  ```bash
  mvn clean package -Dquarkus.native.container-build=true -Pnative 
  ```

- Build container image
  
  ```bash
  docker build -f src/main/docker/Dockerfile.native-distroless -t probe:v1 .
  ```
