package com.example;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Readiness;

import javax.enterprise.context.ApplicationScoped;



@Readiness
@ApplicationScoped
public class AppReadiness implements HealthCheck {
    

    @Override
    public HealthCheckResponse call() {
        // Logger.debug("Readiness Healthcheck");
        if (ApplicationConfig.IS_READY.get())
            return HealthCheckResponse.up("Ready");
        else
            return HealthCheckResponse.down("Ready");
    }
}
