package com.example;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jboss.logging.Logger;
@Path("/")
public class ProbeResource {
    private static final Logger logger = Logger.getLogger(ProbeResource.class);
    @GET
    @Path("/hello")
    @Produces(MediaType.APPLICATION_JSON)
    public Response hello(@HeaderParam("user-agent") String userAgent) {
        if(ApplicationConfig.IS_READY.get()){
            logger.info("User-Agent: "+userAgent+" Status: OK");
            return Response.status(200).encoding("application/json")
            .entity("{\"status\":\"ok\"}")
            .build();
        }else{
            logger.info("User-Agent: "+userAgent+" Status: Down");
            return Response.status(503).encoding("application/json")
            .entity("{\"status\":\"down\"}")
            .build();
        }
    }

    @GET
    @Path("/stop")
    @Produces(MediaType.APPLICATION_JSON)
    public String stopApp(@HeaderParam("user-agent") String userAgent) {
        ApplicationConfig.IS_READY.set(false);
        logger.info("User-Agent: "+userAgent+" Set /hello to return 503");
        return "{\"status\":\"ok\"}";
    }
    @GET
    @Path("/start")
    @Produces(MediaType.APPLICATION_JSON)
    public String startApp(@HeaderParam("user-agent") String userAgent) {
        ApplicationConfig.IS_READY.set(true);
        logger.info("User-Agent: "+userAgent+" Set /hello to return 200");
        return "{\"status\":\"ok\"}";
    }
}