package com.example;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;


@QuarkusTest
public class ProbeResourceTest {

    @Test
    public void testEndpoint() {
        given()
          .when().get("/hello")
          .then()
             .statusCode(200);
    }

}